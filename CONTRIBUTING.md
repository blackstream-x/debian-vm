# Contributing (de)

:+1: :tada: Zunächst vielen Dank, dass du dir die Zeit nimmst
und hier mitwirken möchtest! :tada: :+1:

## Wie genau kann ich mitwirken?

### Eine Ergänzung anregen

Erstelle ein neues Issue mit Titel "Ergänzung" oder "Anregung"
und beschreibe deine Anregung dort.

Du kannst im Issue-Text auch direkt MarkDown verwenden.

### Einen Fehler melden

Auch das funktioniert über ein neues Issue,
am Besten mit "Fehler" o.ä. im Titel.

### Direkt an der Dokumentation mitschreiben

:+1: Hervorragend, das hilft am Meisten!

Allerdings bitte ich dich, dabei ein paar Dinge zu beachten.

#### Umgang mit Git bzw. GitLab

*   Immer in einem eigenen Branch arbeiten.
*   Für Bilder bitte immer [Git-LFS](https://git-lfs.github.com/) verwenden.
*   Änderungen möglichst lokal durchführen und dort ausprobieren
    (MkDocs stellt einen [lokalen Entwicklungsserver](howto/local-dev-server.md)
    zur Verfügung).
*   Kleinere Änderungen können auch über die Web-IDE durchgeführt werden.
    Diese bietet auch eine Vorschau für MarkDown an
    ("Preview Markdown" über dem Editierfenster direkt neben "Edit").
*   Erst wenn alles veröffentlichungsreif ist
    (Rechtschreibung, Grammatik, Darstellung, Inhalt sachlich korrekt),
    den Branch in den Default-Branch (`main`) mergen.
*   Zur Veröffentlichung ein (Git-)Tag
    (nach [Semantic Versioning](https://semver.org/lang/de/spec/v2.0.0.html)
    mit kleinem `v` direkt vor der Version, z.B. `v0.6.7`)
    auf den Default-Branch setzen.
*   **Dinge ausprobieren:** :-1: mit Git bitte nicht. 
    Wenn du dir unsicher bist, frag bitte jemanden, der sich gut mit Git auskennt,
    bevor du "herumprobierst". 
    Die allermeisten Situationen können mit Git gelöst werden,
    aber diese Lösung ist nicht in jedem Fall offensichtlich.
*   GitLab bietet unter der Menüpunkt "Repository :arrow_right: Graph"
    eine grafische Übersicht der Branches an.

#### Umgang mit MarkDown-Dateien

*   [MarkDown](howto/markdown.md)-Dateien im Verzeichnis [docs](docs/)
    erstellen bzw. bearbeiten.
    Weitere Informationen siehe [HowTo: MkDocs](howto/mkdocs.md).
*   Einen geeigneten Editor mit Syntax-Highlighting für MarkDown verwenden
    *   Linux: so gut wie jeder Editor
    *   Mac: _(da habe ich zu wenig Wissen, vermutlich ähnlich wie bei Linux)_
    *   Windows: 
        *   Es gibt diverse geeignete Editoren und IDEs,
            darunter VS Code.
        *   UltraEdit bietet neben dem Syntax-Highlighting
            eine Live-Vorschau für MarkDown.
        *   :no_entry_sign: NotePad, WordPad, OneNote, Word etc.
            sind **nicht** geeignet!
*   Text mit Überschriften strukturieren
    :arrow_right: Vorteil: Abschnitte werden verlinkbar.
*   Weitgehender Verzicht auf Tabellen,
    denn das Meiste lässt sich z.B. mit Unterabschnitten
    oder Listen übersichtlicher darstellen und versionieren.
*   Formatierung: Weniger ist mehr.
    Markdown hat _bewusst_ nur eine kleine Auswahl an Formatierungsoptionen.
*   **Dinge ausprobieren:** :+1: ja, sehr gerne, aber bitte:
    *   Lokal mit direkter Vorschau
        (am Besten über den [lokalen Entwicklungsserver](howto/local-dev-server.md))
    *   im eigenen Branch

