# Debian-VM

Anleitung für die Installation und Konfiguration von Debian Unstable
in einer lokalen VM mit qemu-KVM

URL: <https://blackstream-x.gitlab.io/debian-vm/>

## Mitmachen

siehe [CONTRIBUTING](CONTRIBUTING.md)
