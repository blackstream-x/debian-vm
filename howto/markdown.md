# HowTo: Markdown

Diese Beschreibung versteht sich als eine gekürzte deutsche Übersetzung
der Original-Syntaxbeschreibung für MarkDown
(<https://daringfireball.net/projects/markdown/syntax>)
mit einigen wenigen Ergänzungen.

Eine stark verkürzter Überblick steht als
[MarkDown kompakt](markdown-kompakt.md) bereit.


## Einleitung

### Philosophie

> Markdown is intended to be as easy-to-read and easy-to-write as is feasible.

Markdown soll so einfach zu lesen und zu schreiben sein wie möglich.

Dabei steht die _Lesbarkeit_ im Vordergrund.


### Eingebettetes HTML

HTML-Code _kann_ in MarkDown-Quelltext eingebettet werden,
allerdings wird dieser dadurch aufgebläht und schwerer lesbar,
was wiederum der MarkDown-Philosophie widerspricht.

HTML-Block-Elemente müssen von Leerzeilen umgeben sein.


### Spezialzeichen

[HTML-Zeichenreferenzen](https://html.spec.whatwg.org/multipage/named-characters.html#named-character-references)
wie z.B. `&copy;` für **&copy;** können einfach so geschrieben werden.

Ein `&`, das nicht Teil eine Zeichenreferenz ist, wird hingegen auch als **&** ausgegeben.

Soll einem Zeichen (wie zum Beispiel einem `*`) die Spezialbedeutung genommen werden,
so kann das durch einen vorangestellten Backslash erreicht werden (in diesem Fall: `\*`).


## Block-Elemente

### Überschriften

Überschriften strukturieren ein Dokument und werden
z.B. in GitLab für ein automatisches Inhaltsverzeichnis verwendet.

Es gibt zwei mögliche Notationen (Setext und atx), wobei die Nutzung des
atx-Stils verbreiteter ist.

**Setext-Stil**

Überschriften werden durch "Unterstreichen" einer Textzeile
mit `=` (1. Grad) bzw. `-` (2. Grad) definiert.

Es können nur Überschriften ersten und zweiten Grades erzeugt werden:

```
Überschrift 1. Grades
=====================

Überschrift 2. Grades
---------------------
```

**atx-Stil**

Überschriften werden durch 1–6 `#` am Zeilenanfang erzeugt,
womit Überschriften ersten bis sechsten Grades unterstützt werden.

```
# Überschrift 1. Grades

## Überschrift 2. Grades

###### Überschrift 6. Grades
```

Beliebig viele `#` können aus optischen Gründen ans Ende der Überschrift
angefügt werden. Diese werden im Ergebnis nicht dargestellt.


### Absätze

Ein Absatz besteht einfach aus einer oder mehreren aufeinander folgenden
Textzeilen, die durch Leerzeilen begrenzt sind.
Normale Absätze sollen nicht mit Leerzeichen oder Tabs eingerückt werden.

Innerhalb eines Absatzes spielen Zeilenumbrüche keine Rolle,
sie werden in Leerzeichen umgewandelt.

Ein erzwungener Zeilenumbruch innerhalb eines Absatzes kann mit
mindestens 2 Leerzeichen am Ende einer Zeile erreicht werden.


### Blockzitate

Blockzitate werden mit `> ` am Anfang jeder Zeile definiert, ähnlich wie in E-Mails:

**Das Beispiel**

```
> Dies ist ein Blockzitat mit zwei Absätzen.
> Der erste erstreckt sich im Quelltext
> über mehrere Zeilen.
>
> Dies ist der zweite Absatz.
```

**erzeugt folgende Ausgabe:**

> Dies ist ein Blockzitat mit zwei Absätzen.
> Der erste erstreckt sich im Quelltext
> über mehrere Zeilen.
>
> Dies ist der zweite Absatz.

Blockzitate können verschachtelt werden oder auch andere Blockelemente,
einschließlich Überschriften, enthalten.


### Listen

Ungeordnete (d.h. Strich- bzw. Punkt-) Listen nutzen `*`, `+` oder `-`
am Zeilenanfang, geordnete eine Zahl gefolgt von einem Punkt, z.B. `1.`.

#### ungeordnete Listen

**Das Beispiel**

```
+ Rot
+ Grün
+ Blau
```

**wird wie folgt dargestellt:**

+ Rot
+ Grün
+ Blau

#### nummerierte Listen

:bulb: Die erste Zahl in der Liste bestimmt die Startnummer,
bei den Folgezeilen ist deren jeweilige Zahl irrelevant.

**Das Beispiel**

```
7. Hund
9. Katze
4. Maus
```

**erzeugt die folgende Ausgabe:**

7. Hund
9. Katze
4. Maus

#### Verschachtelung

Listen können verschachtelt werden und z.T. auch andere Blockelemente
enthalten. Details dazu: <https://daringfireball.net/projects/markdown/syntax#list>


### Codeblöcke

Codeblöcke bestehen aus einer oder mehreren aufeinander folgenden Zeilen,
von denen jede einzelne mit mindestens 4 Leerzeichen beginnt
(es sei denn, diese Einrückung wäre zum Beispiel durch eine Liste bedingt).

Einrückungen unterhalb der Haupt-Einrückungsebene werden beibehalten.

**Das Beispiel**

```
    def main(arguments: Optiona[List[str]]) -> int:
        """Main function"""
        ...
        return returncode
```

**erzeugt die folgende Ausgabe:**

    def main(arguments: Optiona[List[str]]) -> int:
        """Main function"""
        ...
        return returncode


#### Erweiterung "Fenced Code Blocks"

GitLab, GitHub, MkDocs und viele andere Plattformen bzw. Programme unterstützen
die Erweiterung "Fenced Code Blocks".

Dabei wird der Codeblock nicht eingerückt, sondern so, wie er ist,
zwischen zwei Zeilen geschrieben, die jeweils nur 3 Backticks enthalten:

**Das Beispiel**

    ```
    Tiere:
      Nagetiere:
        - Mäuse
        - Kaninchen
      Raubtiere:
        - Hunde
        - Katzen
    ```

**wird dargestellt als:**

```
Tiere:
  Nagetiere:
    - Mäuse
    - Kaninchen
  Raubtiere:
    - Hunde
    - Katzen
```


Die eröffnenden 3 Backticks dürfen direkt von einem Schlüsselwort,
das den Quelltext bezeichnet, gefolgt werden.
Falls es dafür Syntax-Highlighting-Regeln gibt,
werden diese bei der Darstellung angewandt.

**Das Beispiel**

    ```yaml
    Tiere:
      Nagetiere:
        - Mäuse
        - Kaninchen
      Raubtiere:
        - Hunde
        - Katzen
    ```

**wird dargestellt als:**

```yaml
Tiere:
  Nagetiere:
    - Mäuse
    - Kaninchen
  Raubtiere:
    - Hunde
    - Katzen
```

### Horizontale Trennlinien

3 oder mehr Bindestriche (`-`), Sterne (`*`) oder Unterstriche (`_`) auf einer
eigenen Zeile erzeugen eine horizontale Trennlinie.


## Inline-Elemente

### Betonung: kursiv oder fett

Unmittelbar zwischen einfachen Sternen (`*`) oder Unterstrichen (`_`)
eingeschlossener Text erzeugt _einfache Betonung_
(HTML: `<em>`, im allgemeinen kursiv dargestellt).

Zwischen jeweils doppelten Sternen(`**`) oder Unterstrichen (`__`) eingeschlossener
Text wird **stark betont** (HTML: `<strong>`, üblicherweise Fettschrift).

**Das Beispiel**

```
_einfach_ oder **stark** betonter Text
```

**erzeugt die folgende Ausgabe:**

_einfach_ oder **stark** betonter Text


### Code

Code wird in Backticks(`` ` ``) eingeschlossen.
In der Ausgabe wird das HTML-Tag `<code>` zur Darstellung verwendet,
was normalerweise in Monospace-Schrift mit Umrandung resultiert.

**Das Beispiel**

```
(...) die Funktion `main()` verwenden (...)
```

**erzeugt die folgende Ausgabe:**

(...) die Funktion `main()` verwenden (...)


### Links

#### Inline-Stil

Inline-Links werden wie folgt definiert:

Link-Text eingeschlossen von eckigen Klammern,
gefolgt von einem runden Klammerpaar, das die URL und optional
einen mit einem Leerzeichen getrennten, in Anführungszeichen eingeschlossenen
Titel enthält.

**Das Beispiel**

```
- [MarkDown](https://daringfireball.net/projects/markdown/ "Offizielle MarkDown-Seite")
- [MkDocs ohne title-Attribut](https://www.mkdocs.org/)
```

**erzeugt folgende Ausgabe:**

- [MarkDown](https://daringfireball.net/projects/markdown/ "Offizielle MarkDown-Seite")
- [MkDocs ohne title-Attribut](https://www.mkdocs.org/)


:bulb: Als URLs können auch relative Pfade angegeben werden, so dass
z.B. auch  verschiedene MarkDown-Dokumente miteinander verlinkt werden können.


#### Referenzstil

Insbesondere wenn ein Link mehrmals im Dokument auftaucht,
bietet sich der Referenzstil an.
Dabei wird der Linktext wie gehabt in eckigen Klammern notiert,
allerdings gefolgt von der Referenz auf einen Namen.

Dieser Name muss an irgendeiner Stelle im Dokument definiert werden,
indem _auf einer Zeile für sich_ dieser Name in eckigen Klammern,
unmittelbar gefolgt von einem Doppelpunkt, einem Leerzeichen,
der URL und optional einem Titel in Anführungszeichen notiert wird.

Die Definition des Links wird dabei im fertigen Dokument
nicht explizit ausgegeben.


**Dieses Beispiel**

```
- [MarkDown][markdown]
- [MkDocs ohne title-Attribut][mkdocs]

[markdown]: https://daringfireball.net/projects/markdown/ "Offizielle MarkDown-Seite"
[mkdocs]: https://www.mkdocs.org/
```

**erzeugt über den Referenzstil die gleiche Ausgabe wie das vorige Beispiel mit dem Inline-Stil:**

- [MarkDown][markdown]
- [MkDocs ohne title-Attribut][mkdocs]

[markdown]: https://daringfireball.net/projects/markdown/ "Offizielle MarkDown-Seite"
[mkdocs]: https://www.mkdocs.org/


#### Automatic links

Dabei wird die URL zwischen Kleiner- und Größer-Zeichen (`<>`) notiert.

**Das Beispiel**

```
<https://daringfireball.net/projects/markdown/>
```

**erzeugt folgende Ausgabe:**

<https://daringfireball.net/projects/markdown/>


### Bilder

Bilder werden ähnlich wie Links im
[Inline-](#inline-stil) oder [Referenzstil](#referenzstil) definiert,
jedoch mit vorangestelltem Ausrufungszeichen (`!`).
Dabei wird der Linktext zum
[Alternativtext](https://bik-fuer-alle.de/alternativtexte-fuer-grafiken.html)
für das Bild.
MarkDown forciert somit in diesem Fall Barrierefreiheit über die Syntax.

**Das Beispiel**

```
![Maastricht in der Abenddämmerung](maastricht_at_dusk.jpg "Maastricht in de schemering")
```

**erzeugt folgende Ausgabe:**

![Maastricht in der Abenddämmerung](maastricht_at_dusk.jpg "Maastricht in de schemering")


## Ergänzungen

_(tba: Tabellen, Emoji, Admonitions)_

