# HowTo: MarkDown kompakt

_kurz und knapp jeweils anhand eines Beispiels_ 

## Gliederung / Überschriften

```
# Überschrift 1. Ordnung

## Überschrift 2. Ordnung

(...)

###### Überschrift 6. Ordnung
```


## Absätze

```
Dies
ist
ein
Absatz

Weiterer Absatz
```

## Blockzitate

```
> zitierter Text
```

## Listen

### Strich- bzw. Punktlisten

```
- Äpfel
- Birnen
```

### Nummerierte Listen

```
1. Vorbereitung
2. Planung
3. Durchführung
```

## Codeblöcke

```
    vorformatiert
    dargestellter
    Text
```

## Horizontale Trennlinien

```
---
```

## Texthervorhebung

### Kursiv

```
_einfach betonter (kursiv dargestellter) Text_
```

### Fett

```
**stark betonter (fett dargestellter) Text**
```

## Inline-Code

```
Darstellung kurzer `Code-Schnipsel` im Text
```

## Links

```
[Linktext](Ziel-URL "optionaler Titel")
```

## Eingebundene Bilder

```
![Alternativtext](Bild-URL "optionaler Titel")
```

