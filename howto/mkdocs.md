# HowTo: MkDocs

:information_source: [MkDocs: User Guide / Writing Your Docs](https://www.mkdocs.org/user-guide/writing-your-docs/)

Die Dokumente werden im Verzeichnis [docs](../docs/) als
[MarkDown](markdown.md)-Dateien erstellt.
Die Verzeichnisstruktur darunter gibt die Seitenstruktur
der generierten Dokumentation vor.
Die Datei **index.md** wird in der Navigation jeweils
als erste Seite verlinkt, der Rest folgt alphabetisch
(genauer gesagt nach
[ASCIIbetischer Reihenfolge](https://en.wikipedia.org/?title=ASCIIbetical_order))
sortiert.

Die in [Python-Markdown: Extensions](https://python-markdown.github.io/extensions/)
aufgeführten Erweiterungen können nach Aktivierung in [mkdocs.yml](../mkdocs.yml)
(siehe <https://www.mkdocs.org/user-guide/configuration/#markdown_extensions>)
genutzt werden.

Standardmäßig sind davon die Erweiterungen
[fenced_code](https://python-markdown.github.io/extensions/fenced_code_blocks/) 
und [tables](https://python-markdown.github.io/extensions/tables/),
welche beide ebenso in der GitLab-Vorschau dargestellt werden,
aktiviert, ohne dass es weiterer Konfiguration bedarf.

## Unterschiede zur MarkDown-Darstellung in GitLab

### Verwendung von Emoji

GitLab unterstützt Emoji von Haus aus, siehe
<https://docs.gitlab.com/ee/user/markdown.html#emojis>
in Verbindung mit <https://www.webfx.com/tools/emoji-cheat-sheet/>.

Für die Darstellung von Emoji in durch MkDocs erzeugten Seiten
ist die Installation des Python-Package 
[pymdown-extensions](https://pypi.org/project/pymdown-extensions/)
und die Aktivierung der Extension `pymdownx.emoji` in [mkdocs.yml](../mkdocs.yml)
notwendig, siehe <https://facelessuser.github.io/pymdown-extensions/extensions/emoji/>).

### Anzeige von Admonitions

MkDocs unterstützt
[Admonitions](https://python-markdown.github.io/extensions/admonition/),
GitLab kann diese in der MarkDown-Vorschau jedoch nicht
in ähnlicher Form darstellen.

