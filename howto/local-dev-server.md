# HowTo: Lokaler Entwicklungsserver

## Warum?

Mit dem in MkDocs eingebauten lokalen Entwicklungsserver
lassen sich Änderungen in den Markdown-Dateien quasi live verfolgen,
bevor die Änderungen ins zentrale Repository gepusht werden.

## Wie?

Der Start des Entwicklungsservers ist auch in der MkDocs-Dokumentation
unter <https://www.mkdocs.org/getting-started/#creating-a-new-project>
beschrieben, im Prinzip ist es aber ganz einfach: der Befehl

```
mkdocs serve
```

startet den Server, und dieser gibt die URL aus, unter der die Seiten
lokal zur Verfügung gestellt werden. Falls nicht anders parametrisiert,
handelt es sich dabei um die URL <http://127.0.0.1:8000/>.


## Voraussetzungen

Python 3 und das Paket [mkdocs](https://pypi.org/project/mkdocs/) müssen
auf der Workstation installiert sein.
Falls weitere Pakete benötigt werden
(z.B. [pymdown-extensions](https://pypi.org/project/pymdown-extensions/)
 für die [Verwendung von Emoji](mkdocs.md#verwendung-von-emoji)),
müssen diese ebenfalls installiert sein.

Idealerweise installierst du die Pakete in einem
[Virtual Environment](https://docs.python.org/3/tutorial/venv.html)
mit dem Befehl `pip install`.

In diesem Projekt stehen diese Pakete in der Datei
[requirements.txt](../requirements.txt), der Befehl zur Installation
lautet dementsprechend:

```
pip install -r requirements.txt
```

