# Disk-Image-Vorlagen erzeugen

-   Debian Stable (hier: bullseye 11.5)
-   Debian Unstable

_(je Release nur einmal notwendig)_


## Basis-Image herunterladen

Das AMD64-Netinstall-ISO von der offiziellen [Downloadseite](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/) herunterladen und speichern.


## Vor der Installation folgende Dinge merken oder aufschreiben:

-   vorläufiger Hostname (gleichzeitig im Namen des Disk-Image
    und Name der vorläufigen VM) → hier: `template`
-   vorläufiger Adminbenutzer und dazugehöriges Passwort → hier: alles `debian`
-   gewünschte Größe der Festplatte (hier: 10 GiB)


## Eine minimale VM mit diesem Image installieren

-   Im virt-manager eine neue VM einrichten:
    -   Installation vom gerade heruntergeladenen ISO-Image
    -   Typ: automatisch ermittelten Typ oder "Debian 11" verwenden
    -   Festplatte: mit "Benutzerdefinierten Speicher anlegen" eine neue anlegen,
        an einem Ort, an den du mit deinem User herankommst.
        Größe: wie vorher festgelegt (hier: 10 GiB)
    -   Namen der neuen VM festlegen (Achtung, hier wird automatisch der
        ausgewählte Betriebsyystemtyp vorgeschlagen) → auf den vorläufigen
        Hostname ändern
    -   Netzwerk: auf Default (NAT) belassen.
-   Maschine anlegen und mit dem Installationsprozess beginnen,
    dabei den textbasierten Installer (`Install`) wählen.
-   Fragen entsprechend beantworten:
    -   Rechnername wie vorab festgelegt (hier: `template`) einstellen
    -   Domain name: `vm`
    -   Root-Passwort leer lassen
    -   Username und Passwort des vorab gewählten Benutzers setzen
    -   Festplatte: geführte Partitionierung - vollständige Festplatte verwenden,
        eine einzige Partition,
        und entsprechend bestätigen.
    -   Warten, bis das Basissystem installiert ist
    -   Popularity-contest abwählen
    -   Alle Optionen bei der Paketauswahl abwählen
    -   wieder abwarten, Bootloader auf dem primären Laufwerk
        (`/dev/vda` auswählen) installieren
    -   `<weiter>` bestätigen, es erfolgt der Reboot ins neue System


## Nacharbeiten nach der Grundinstallation

Nach dem Neustart mit dem neuen Benutzer anmelden
und mit `sudo -i` root werden.

Den Apt-Cache aktualisieren und
zusätzliche Pakete installieren:

```text
apt update
apt install ca-certificates dbus
```

Die VM wieder herunterfahren:

```text
init 0
```


## Festplattenimage für Stable als Template duplizieren

auf dem Hostsystem mit `qemu-img` eine größenoptimierte Kopie
in einem Quellverzeichnis (z.B. dort, wo da ISO-Image liegt)
erzeugen.

Möglicherweise ist es notwendig, dem eigenen User Leserechte
auf des zu kopierende Image einzuräumen:

```text
sudo chmod o+r medien/vms/template.qcow2
```


Der Name sollte das Release, den gewählten Hostnamen, die Größe
und ggf. auch den User beinhalten.

```text
qemu-img convert -f qcow2 -O qcow2 -o preallocation=off medien/vms/template.qcow2 Downloads/debian/bullseye-11.5-template-10g_user_debian.qcow2
```


## Umstellung auf Unstable

Den Gast wieder hochfahren und die Paketquellen auf `unstable` umstellen:

![Umstellung der Paketquellen auf "unstable" mittels sed](vm-sources-auf-unstable-umstellen.png)

Die Paketumstellung mit `apt dist-upgrade` durchführen

```text
apt update
apt dist-upgrade
apt --purge autoremove
```

Die (jetzt unstable-)VM neu booten

```text
init 6
```


## Festplattenimage für Unstable als Template duplizieren

Nach Funktionsprüfung der Unstable-VM diese wieder herunterfahren:

```text
init 0
```

und analog dem obigen Vorgehen ein minimales Image für unstable erzeugen

```text
qemu-img convert -f qcow2 -O qcow2 -o preallocation=off medien/vms/template.qcow2 Downloads/debian/sid-template-10g_user_debian.qcow2
```


## Temporäre VM wieder löschen

Die temporäre VM wird nun nicht mehr gebraucht und kann samt Festplattenimage
(template.qcow2) aus dem virt-manager entfernt werden.

Jetzt können aus den mit `qemu-img` erzeugten Disk-Images neue Disk-Images durch einfaches Kopieren erzeugt werden.

Bitte _nicht_ die Template-Images in QEMU direkt verwenden,
dadurch würden sie gegenüber dem aktuellen Stand verändert.

