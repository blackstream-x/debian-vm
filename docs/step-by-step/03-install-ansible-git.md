# Ansible und Git installieren

## Stable: Pakete ansible und git

```text
sudo apt install --no-install-recommends ansible git
```

## Unstable: Pakete ansible-core und git

```text
sudo apt install --no-install-recommends ansible-core git
```


## Dieses Repository klonen

hier: nach ~/git

```text
mkdir git
cd git
git clone https://gitlab.com/blackstream-x/debian-vm.git
cd debian-vm
```

