# QEMU/KVM auf dem Hostsystem installieren

_- einmalig -_

## Benötigte Debian-Pakete

-   qemu-system-x86
-   qemu-utils
-   virt-manager
-   virt-viewer

Alle Pakete können mit `apt install` installiert werden:

```text
sudo apt install qemu-system-x86 qemu-utils virt-manager virt-viewer
```

## Benötigte Berechtigungen

Den eigenen User der Gruppe `libvirt` hinzufügen

```text
sudo adduser $USER libvirt
```

und anschließend ab- und wieder anmelden, damit die Berechtigungn greift.

