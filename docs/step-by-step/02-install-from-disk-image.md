# Eine neue VM aus einem Disk-Image erzeugen

## Vorab: Namen überlegen, merken und/oder aufschreiben

- neuer Hostname (Platzhalter: `newhostname`)
- neuer User (Platzhalter: `newuser`)

## Dateien kopieren

Eine der im [vorigen Schritt](01-create-disk-images.md)
erzeugten Disk-Image-Vorlagen
kannst du einfach mit einem normalen Kopierbefehl
an den Ort, an den sowohl du mit deinem User
als auch QEMU herankommen, kopieren.

Als Name bietet sich der **neue Hostname** an.

In virt-manager eine neue VM mit diesem Image als Festplatte erstellen.
Bei der Betriebssystemauswahl je nach Image `Debian 11` oder
`debiantesting` angeben.

Im Abschlussschritt noch die VM mit dem **neuen Hostnamen** benennen,
sonst übernimmt QEMU das ausgewählte Betriebssystem als Namen.

## Grundlegende Einrichtung

Login auf der Textkonsole mit dem im Disk-Image angelegten User
(`debian`) und Switch auf Root mit 

```text
sudo -i
```

### Hostname setzen

Dabei den **neuen Hostnamen** verwenden.

```text
hostnamectl set-hostname newhostname
```

!!! note "ggf. dbus starten"

    Falls eine Fehlermeldung

    ```Failed to connect to bus: Datei oder Verzeichnis nicht gefunden```

    erscheint, muss vor einem erneuten Absetzen des Befehls
    **dbus** gestartet werden:

    ```systemctl start dbus```

In `/etc/hosts` den Hostnamen in der entsprechenden Zeile
(dort sollte noch `template` stehen)
auf den **neuen Hostnamen** setzen.

Das kann z.B. mit `vi`, aber auch z.B. mit `sed` geschehen.

### User einrichten

Den oben gemerkten **neuen User** mit

```text
adduser newuser
```

einrichten. Dabei werden diverse Angaben abgefragt,
siehe [Screenshot](adduser.png)

Mit

```text
adduser newuser sudo
```

werden dem User sudo-Berechtigungen verliehen.

Ausloggen,
als der neu eingerichtete User wieder einloggen
und testweise mit `sudo -i` root werden.

Den User aus der Disk-Image-Vorlage kannst du jetzt löschen:

```text
deluser --remove-home debian
```


## Netzwerkverbindung vom Hostsystem zum Gastsystem einrichten

### Gastsystem: IP-Adresse ermitteln

```
ip addr
```

Maschine **auf dem Hostsystem** in `/etc/hosts` eintragen

_jeweils mit dem **neuen Hostnamen**_

```text
<ermittelte Adresse>    newhostname.vm  newhostname
```

### Gastsystem: SSH-Server installieren

!!! note ""

    Das Textinterface in der VM unterstützt noch kein Copy und Paste.
    Um dennoch nicht den Befehl komplett abtippen zu müssen,
    kannst du dich nach dieser Einrichtung vom Hostsystem dorthin
    einloggen und dann Copy/Paste verwenden.


```text
apt install openssh-server
```

### Hostsystem: SSH Public Keys auf das Gastsystem kopieren

Dabei den **neuen Usernamen** und den **neuen Hostnamen** verwenden.

```text
ssh-copy-id newuser@newhostname.vm
```

Nach Übertragung des/der Keys auf dem Gastsystem einloggen:

```text
ssh newuser@newhostname.vm
```

Hierbei sollte keine Passwortabfrage mehr erfolgen.

!!! note ""

    Sowohl die Installation der Desktopumgebung
    als auch der Reboot können aus dieser SSH-Session erfolgen.
    

## Desktopumgebung installieren

### Stable: mit Firefox ESR

```text
sudo apt install curl firefox-esr firefox-esr-l10n-de fonts-stix fonts-lmodern xfce4 xfce4-goodies spice-vdagent
```

### Unstable: mit dem aktuellsten Firefox

```text
sudo apt install curl firefox firefox-l10n-de fonts-stix fonts-lmodern xfce4 xfce4-goodies spice-vdagent
```

## VM rebooten

Nach dem Reboot mittels 

```text
sudo reboot
```

startet die VM in die grafische Oberfläche.

## Energieverwaltung anpassen

Nach Login über Anwendungen → Einstellungen → Energieverwaltung
die folgenden Einstellungen vornehmen:

![Bildschirmenergieverwaltung ausschalten](energy-screen.png "Bildschirmenergieverwaltung ausschalten")

![Sitzung nie sperren](energy-security.png "Sitzung nie sperren")


## Weitere Schritte

Zusatzsoftware nach Bedarf installieren.
Falls diese Installation über ansible läuft,
solltest du nun [ansible und git installieren](03-install-ansible-git.md).

Regelmäßige Updates erfolgen am Einfachsten mit folgender Befehlskombination:

```text
sudo apt update && sudo apt upgrade && sudo apt --purge autoremove
```

