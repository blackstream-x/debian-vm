# makedeb

_Werkzeug zur Erzeugung von Debian-Paketen_

## Quelle

<https://docs.makedeb.org/installing/apt-repository/>

## Installationsvorgang

**Auf dem Gastsystem** in das Verzeichnis wechseln,
in das dieses Repository geklont wurde,
und das Ansible-Playbook ansible/install-makedeb.yaml
mit der Option `--ask-become-pass` ausführen:

```text
rainer@funchal:~/git/debian-vm$ ansible-playbook --ask-become-pass ansible/install-makedeb.yaml
```

## Erfolgskontrolle

Nach erfolgreichem Ausführen des Playbooks sollte
der Befehl makedeb zur Verfügung stehen.

