# oh-my-git

_Abgewandeltes Prompt für Git-Repositories_

## Quelle

<https://github.com/arialdomartini/oh-my-git>

## Installationsvorgang

**Auf dem Gastsystem** in das Verzeichnis wechseln,
in das dieses Repository geklont wurde,
und das Ansible-Playbook ansible/install-oh-my-git.yaml ausführen:

```text
rainer@funchal:~/git/debian-vm$ ansible-playbook ansible/install-oh-my-git.yaml
```

## Erfolgskontrolle

Nach erfolgreichem Ausführen des Playbooks sollte
als monospace-Schriftart `SourceCodePro+Powerline+Awesome Regular`
eingestellt sein.

In Git-Repositories sollte das Prompt wechseln,
wie aus folgendem Screenshot ersichtlich:

![oh-my-git-Prompt in einem Verzeichnis mit einem Git-Repository](omg-in-action.png) 

