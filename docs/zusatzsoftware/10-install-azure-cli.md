# Azure CLI

_Command Line interface für Azure_

## Quelle

<https://learn.microsoft.com/de-de/cli/azure/install-azure-cli-linux?pivots=apt>

!!! caution ""

    Micosoft unterstützt zz. als neueste Version **bullseye**,
    d.h. das aktuelle Stable-Release von Debian. 


## Installationsvorgang

**Auf dem Gastsystem** in das Verzeichnis wechseln,
in das dieses Repository geklont wurde,
und das Ansible-Playbook ansible/install-azure-cli.yaml
mit der Option `--ask-become-pass` ausführen:

```text
rainer@funchal:~/git/debian-vm$ ansible-playbook --ask-become-pass ansible/install-azure-cli.yaml
```

## Erfolgskontrolle

Nach erfolgreichem Ausführen des Playbooks sollte
der Befehl `az` über die Kommandozeile aufrufbar sein
und `az --version` sollte die aktuelle Version ausgeben.

