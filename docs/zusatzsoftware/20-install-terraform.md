# HashiCorp Terraform

_Werkzeug zur Provisionierung von Cloud-Komponenten ([Infrastructure as code](https://en.wikipedia.org/wiki/Infrastructure_as_code))_

## Quelle

<https://www.terraform.io/downloads>

!!! caution ""

    Als neueste Version wird zz. **bookworm**,
    d.h. das aktuelle Testing-Release von Debian, unterstützt. 


## Installationsvorgang

**Auf dem Gastsystem** in das Verzeichnis wechseln,
in das dieses Repository geklont wurde,
und das Ansible-Playbook ansible/install-terraform.yaml
mit der Option `--ask-become-pass` ausführen:

```text
rainer@funchal:~/git/debian-vm$ ansible-playbook --ask-become-pass ansible/install-terraform.yaml
```

## Erfolgskontrolle

Nach erfolgreichem Ausführen des Playbooks sollte
der Befehl `terraform` über die Kommandozeile aufrufbar sein
und `terraform --version` sollte die aktuelle Version ausgeben.

