# Helm

_Paketverwaltung für Kubernetes_

## Quellen

- <https://helm.sh/docs/intro/install/#from-apt-debianubuntu>
- <https://helm.baltorepo.com/stable/debian/>


## Installationsvorgang

**Auf dem Gastsystem** in das Verzeichnis wechseln,
in das dieses Repository geklont wurde,
und das Ansible-Playbook ansible/install-helm.yaml
mit der Option `--ask-become-pass` ausführen:

```text
rainer@funchal:~/git/debian-vm$ ansible-playbook --ask-become-pass ansible/install-helm.yaml
```

## Erfolgskontrolle

Nach erfolgreichem Ausführen des Playbooks sollte
der Befehl `helm` über die Kommandozeile aufrufbar sein
und `helm version` (sic!) sollte die aktuelle Version ausgeben.

