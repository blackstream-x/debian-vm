# VSCodium

_Entwicklungsumgebung VSCode ohne Microsoft-Branding und Telemetrie_

## Quelle

<https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo>

## Installationsvorgang

**Auf dem Gastsystem** in das Verzeichnis wechseln,
in das dieses Repository geklont wurde,
und das Ansible-Playbook ansible/install-codium.yaml
mit der Option `--ask-become-pass` ausführen:

```text
rainer@funchal:~/git/debian-vm$ ansible-playbook --ask-become-pass ansible/install-codium.yaml
```

## Erfolgskontrolle

Nach erfolgreichem Ausführen des Playbooks sollte
VSCodium im Menü unter Anwendungen → Entwicklung aufrufbar sein.

