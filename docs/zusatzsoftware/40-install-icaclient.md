# Citrix Workspace App (aka ICAClient)

_Anwendung zum Desktopzugriff im Firmenumfeld_

!!! note "Stable verwenden"

    Hier bietet sich das Stable-Release an.
    Falls nur der ICA-Client in der VM benutzt werden soll,
    ist es günstiger, eine VM direkt und mit einer etwas
    kleineren virtuellen Festplatte (6 GiB sollten ausreichen)
    zu installieren.


## Quelle

<https://www.citrix.com/downloads/workspace-app/linux/workspace-app-for-linux-latest.html>

## Installationsvorgang

Debian-Paket für x86_64 von der o.a. Seite herunterladen
und mit folgendem Befehl installieren:

```text
sudo apt install --no-install-recommends ./icaclient_22.9.0.21_amd64.deb
```

Die Frage nach der App-Schutzkomponente
mit dem Standardwert (`nein`) beantworten.


## Erfolgskontrolle

Starten einer ICA-Verbindung über die Workspace App

!!! caution "eigener Zertifikatsspeicher"

    Die Workspace App hat - zumindest unter Linux - eine eigene Zertifikatsverwaltung
    und nutzt nicht die systemweite (ca-certificates).

    Dadurch werden viele Zertifikate zunächst nicht akzeptiert,
    und es erscheint eine Fehlermeldung wie folgende:

    ![Fehlermeldung bezüglich nicht vertrautem Zertifikat](icaclient-ssl-error.png)


Angelehnt an die Anleitung 
<https://support.citrix.com/article/CTX231524/citrix-workspace-app-for-linux-how-to-trust-a-ca-certificate>
(allerdings sind dort die Hinweise auf die Dateiendung `.crt` **falsch**,
als Dateiendung muss `.pem` verwendet werden!)
habe ich die Situation für das bei mir nicht akzeptierte Zertifikat
`D-TRUST_Root_Class_3_CA_2_EV_2009` mit den folgenden Befehlen bereinigt bekommen
(die Ursprungsdatei erfüllte bereits alle Voraussetzungen bis auf die Dateiendung:
User und Gruppe root, mode 0644):

```text
sudo cp -p /usr/share/ca-certificates/mozilla/D-TRUST_Root_Class_3_CA_2_EV_2009.crt /opt/Citrix/ICAClient/keystore/cacerts/D-TRUST_Root_Class_3_CA_2_EV_2009.pem
sudo /opt/Citrix/ICAClient/util/ctx_rehash
```
