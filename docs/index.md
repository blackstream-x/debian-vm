# Debian in einer VM

[Schritt-für-Schritt-Anleitung](step-by-step/index.md)

## Wofür eine VM?

Für alle möglichen Tätigkeiten,
die vom Hauptsystem isoliert abgewickelt werden sollen.
Das könnte z.B. auch Onlinebanking sein,
wobei _diese Anleitung_ eher auf Cloud-Management abzielt.

## Warum Debian?

Dafür könnte ich theoretisch 
[Gründe für den Einsatz von Debian](https://www.debian.org/intro/why_debian)
abschreiben.

## Warum Unstable?

Das Unstable-Release erhält zeitnah die neuesten Paketversionen.
Es enthält auch Pakete, die (noch) nicht im
[Stable-Release](https://www.debian.org/releases/stable/) enthalten sind.

Die Warnung auf <https://www.debian.org/releases/sid/>
bezüglich Sicherheitsaktualisierungen ist insofern etwas irreführend,
als dass in Unstable die Sicherheit über die zeitnahe Bereitstellung
neuer Paketversionen gewährleistet wird.

